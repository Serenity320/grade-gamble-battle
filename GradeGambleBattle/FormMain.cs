﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradeGambleBattle
{
    public partial class FormMain : Form
    {
        private Game game;

        public FormMain(int userID)
        {
            InitializeComponent();

            // Initialize
            game = new Game(userID);
            Student user = game.getUser();

            // picStudent.image = 
            txtNumber.Text = Convert.ToString(user.getID());
            txtName.Text = user.getName();
            txtMajor.Text = user.getMajor();
            txtMajor2.Text = user.getMajor2();
            txtYears.Text = Convert.ToString(user.getYears());
            txtToken.Text = String.Format("{0:#,##0}", user.getToken());

            // Panel
            FormStudent frmStudent = new FormStudent(this);

            frmStudent.TopLevel = false;
            frmStudent.AutoScroll = true;
            frmStudent.FormBorderStyle = FormBorderStyle.None;

            pnlMain.Controls.Clear();
            pnlMain.Controls.Add(frmStudent);

            frmStudent.Visible = true;
        }

        public void update()
        {
            Student user = game.getUser();

            // picStudent.image = 
            txtNumber.Text = Convert.ToString(user.getID());
            txtName.Text = user.getName();
            txtMajor.Text = user.getMajor();
            txtMajor2.Text = user.getMajor2();
            txtYears.Text = Convert.ToString(user.getYears());
            txtToken.Text = String.Format("{0:#,##0}", user.getToken());
        }

        public Game getGame()
        {
            return game;
        }

        private void 강의목록ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormStudent frmStudent = new FormStudent(this);

            frmStudent.TopLevel = false;
            frmStudent.AutoScroll = true;
            frmStudent.FormBorderStyle = FormBorderStyle.None;

            pnlMain.Controls.Clear();
            pnlMain.Controls.Add(frmStudent);

            frmStudent.Visible = true;
        }

        private void 수강신청ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSubject frmLecture = new FormSubject(this);

            frmLecture.TopLevel = false;
            frmLecture.AutoScroll = true;
            frmLecture.FormBorderStyle = FormBorderStyle.None;

            pnlMain.Controls.Clear();
            pnlMain.Controls.Add(frmLecture);

            frmLecture.Visible = true;
        }

        private void 성적조회ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormGrade frmGrade = new FormGrade(this);

            frmGrade.TopLevel = false;
            frmGrade.AutoScroll = true;
            frmGrade.FormBorderStyle = FormBorderStyle.None;

            pnlMain.Controls.Clear();
            pnlMain.Controls.Add(frmGrade);

            frmGrade.Visible = true;
        }

        private void 학점배틀ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormGamble frmGamble = new FormGamble(this);

            frmGamble.TopLevel = false;
            frmGamble.AutoScroll = true;
            frmGamble.FormBorderStyle = FormBorderStyle.None;

            pnlMain.Controls.Clear();
            pnlMain.Controls.Add(frmGamble);

            frmGamble.Visible = true;
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
