﻿using System;
using System.Collections; // IEnumerator
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradeGambleBattle
{
    public partial class FormSubject : Form
    {
        private FormMain frmMain;

        public FormSubject(FormMain frmMain)
        {
            InitializeComponent();

            this.frmMain = frmMain;

            update();
        }

        public void update()
        {
            // ListView
            lvwSubject.FocusedItem = null;
            lvwSubject.Clear();

            lvwSubject.View = View.Details;
            lvwSubject.FullRowSelect = true;
            lvwSubject.GridLines = true;

            lvwSubject.Columns.Add("과목번호");
            lvwSubject.Columns.Add("교과목명");
            lvwSubject.Columns.Add("교수이름");
            lvwSubject.Columns.Add("학점");
            lvwSubject.Columns.Add("제한인원");
            lvwSubject.Columns.Add("수강인원");

            List<Subject> subject = frmMain.getGame().getSubject();

            for (IEnumerator i = subject.GetEnumerator(); i.MoveNext(); )
            {
                Subject tmpSubject = (Subject)i.Current;

                ListViewItem item = new ListViewItem(Convert.ToString(tmpSubject.getSubjectID()));
                item.SubItems.Add(tmpSubject.getSubjectName());
                item.SubItems.Add(tmpSubject.getProfessorName());
                item.SubItems.Add(Convert.ToString(tmpSubject.getGrade()));
                item.SubItems.Add(Convert.ToString(tmpSubject.getLimitNum()));
                item.SubItems.Add(Convert.ToString(tmpSubject.getPresentNum()));

                lvwSubject.Items.Add(item);
            }

            foreach (ColumnHeader column in lvwSubject.Columns)
            {
                column.Width = -2;
            }

            this.Controls.Add(lvwSubject);

            // Label
            lblGrade.Text = "수강학점 : " + Convert.ToString(frmMain.getGame().getUser().calSubjectGrade());
        }

        private void btnSubject_Click(object sender, EventArgs e)
        {
            int count = lvwSubject.SelectedItems.Count;

            if (count > 0)
            {
                Subject selSubject = null;

                int idx = lvwSubject.FocusedItem.Index;
                int subjectID = Convert.ToInt32(lvwSubject.Items[idx].SubItems[0].Text);

                List<Subject> subject = frmMain.getGame().getSubject();

                for (IEnumerator i = subject.GetEnumerator(); i.MoveNext(); )
                {
                    Subject tmpSubject = (Subject)i.Current;

                    if (tmpSubject.getSubjectID() == subjectID)
                    {
                        selSubject = tmpSubject;
                    }
                }

                if (selSubject == null)
                {
                    MessageBox.Show("수강신청 오류");
                }
                else
                {
                    bool isAlready = frmMain.getGame().getUser().checkSubject(selSubject);
                    int grade = frmMain.getGame().getUser().calSubjectGrade();

                    int limitNum = selSubject.getPresentNum();
                    int presentNum = selSubject.getPresentNum();

                    if (frmMain.getGame().getUser().isStartMiddelExam())
                    {
                        MessageBox.Show("수강신청 기간 종료");
                    }
                    else if (isAlready)
                    {
                        MessageBox.Show("이미 수강신청한 과목");
                    }
                    else if (grade + selSubject.getGrade() > 19)
                    {
                        MessageBox.Show("수강학점 초과");
                    }
                    else if (limitNum < presentNum)
                    {
                        MessageBox.Show("수강인원 초과");
                    }
                    else
                    {
                        Apply apply = new Apply(frmMain.getGame().getUser().getID(), selSubject, -1, -1, -1);

                        frmMain.getGame().getUser().applySubjet(apply);
                        apply.addApply();

                        MessageBox.Show("수강신청 완료");
                    }
                }
            }

            update();
        }
    }
}
