﻿using System;
using System.Collections.Generic;
using System.Data; // DataTable
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeGambleBattle
{
    public class GameDAO
    {
        private DBConnector dbConnector;

        public GameDAO()
        {
            this.dbConnector = new DBConnector();
        }

        public DataTable getUser(int id)
        {
            DataTable dataTable;

            string query = "select * from student where stdID = " + id;
            dataTable = dbConnector.selectQuery(query);

            dataTable.Columns[0].ColumnName = "stdID";
            dataTable.Columns[1].ColumnName = "stdName";
            dataTable.Columns[2].ColumnName = "stdMajor1";
            dataTable.Columns[3].ColumnName = "stdMajor2";
            dataTable.Columns[4].ColumnName = "stdYears";
            dataTable.Columns[5].ColumnName = "stdGrade";
            dataTable.Columns[6].ColumnName = "stdToken";

            return dataTable;
        }

        public DataTable getsStudent()
        {
            DataTable dataTable;

            string query = "select * from student";
            dataTable = dbConnector.selectQuery(query);

            dataTable.Columns[0].ColumnName = "stdID";
            dataTable.Columns[1].ColumnName = "stdName";
            dataTable.Columns[2].ColumnName = "stdMajor1";
            dataTable.Columns[3].ColumnName = "stdMajor2";
            dataTable.Columns[4].ColumnName = "stdYears";
            dataTable.Columns[5].ColumnName = "stdGrade";
            dataTable.Columns[6].ColumnName = "stdToken";

            return dataTable;
        }

        public DataTable getsSubject()
        {
            DataTable dataTable;

            string query = "select * from subject";
            dataTable = dbConnector.selectQuery(query);

            dataTable.Columns[0].ColumnName = "sbjID";
            dataTable.Columns[1].ColumnName = "sbjName";
            dataTable.Columns[2].ColumnName = "pfName";
            dataTable.Columns[3].ColumnName = "sbjGrade";
            dataTable.Columns[4].ColumnName = "sbjLimitNum";
            dataTable.Columns[5].ColumnName = "sbjPresentNum";

            return dataTable;
        }

        public DataTable getsApplyOfUser(int id)
        {
            DataTable dataTable;

            string query = "select * from apply where stdID = " + id;
            dataTable = dbConnector.selectQuery(query);

            dataTable.Columns[0].ColumnName = "stdID";
            dataTable.Columns[1].ColumnName = "sbjID";
            dataTable.Columns[2].ColumnName = "middleScore";
            dataTable.Columns[3].ColumnName = "finalScore";
            dataTable.Columns[4].ColumnName = "grade";

            return dataTable;
        }

        public DataTable getsBattleOfUser(int id)
        {
            DataTable dataTable;

            string query = "select battle.* from joinbattle join battle on battle.battleID = joinbattle.battleID where joinbattle.stdID = '" + id + "'";

            dataTable = dbConnector.selectQuery(query);

            dataTable.Columns[0].ColumnName = "battleID";
            dataTable.Columns[1].ColumnName = "battleType";
            dataTable.Columns[2].ColumnName = "battingToken";

            return dataTable;
        }

        public DataTable getsJoinOfBattle(int id)
        {
            DataTable dataTable;

            string query = "select * from joinbattle where battleID = '" + id + "'";

            dataTable = dbConnector.selectQuery(query);

            dataTable.Columns[0].ColumnName = "battleID";
            dataTable.Columns[1].ColumnName = "stdID";
            dataTable.Columns[2].ColumnName = "isJoin";
            dataTable.Columns[2].ColumnName = "isEnd";

            return dataTable;
        }
    }
}
