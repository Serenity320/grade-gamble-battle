﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeGambleBattle
{
    public class Apply
    {
        private int userID;
        private Subject subject; // 과목
        private int middleScore; // 중간고사 점수
        private int finalScore; // 기말고사 점수
        private double grade; // 학점

        private ApplyDAO dao;

        public Apply(int userID, Subject subject, int middleScore, int finalScore, double grade)
        {
            this.userID = userID;
            this.subject = subject;
            this.middleScore = middleScore;
            this.finalScore = finalScore;
            this.grade = grade;

            this.dao = new ApplyDAO();
        }

        public void addApply()
        {
            dao.addApply(userID, subject.getSubjectID()); // Database
        }

        public void delApply()
        {
            dao.delApply(userID, subject.getSubjectID()); // Database
        }

        public void calGrade()
        {
            double grade;
            double average;

            average = (middleScore + finalScore) / 2.0;

            if (average >= 90) grade = 4.5;
            else if (average >= 80) grade = 4.0;
            else if (average >= 70) grade = 3.5;
            else if (average >= 60) grade = 3.0;
            else if (average >= 50) grade = 2.5;
            else if (average >= 40) grade = 2.0;
            else if (average >= 30) grade = 1.5;
            else if (average >= 20) grade = 1.0;
            else grade = 0.0;

            this.setGrade(grade);
        }

        public Subject getSubject()
        {
            return this.subject;
        }

        public int getMiddleScore()
        {
            return this.middleScore;
        }

        public void setMiddleScore(int middleScore)
        {
            this.middleScore = middleScore;

            dao.setMiddleScore(userID, subject.getSubjectID(), middleScore); // Database
        }

        public int getFinalScore()
        {
            return this.finalScore;
        }

        public void setFinalScore(int finalScore)
        {
            this.finalScore = finalScore;

            dao.setFinalScore(userID, subject.getSubjectID(), finalScore); // Database
        }

        public double getGrade()
        {
            return this.grade;
        }

        public void setGrade(double grade)
        {
            this.grade = grade;

            dao.setGrade(userID, subject.getSubjectID(), grade); // Database
        }
    }
}
