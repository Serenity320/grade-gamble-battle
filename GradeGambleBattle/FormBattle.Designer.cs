﻿namespace GradeGambleBattle
{
    partial class FormBattle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtStdID = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnSuccess = new System.Windows.Forms.Button();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.lvwStuent = new System.Windows.Forms.ListView();
            this.lblToken = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtStdID
            // 
            this.txtStdID.Location = new System.Drawing.Point(12, 12);
            this.txtStdID.MaxLength = 9;
            this.txtStdID.Name = "txtStdID";
            this.txtStdID.Size = new System.Drawing.Size(100, 21);
            this.txtStdID.TabIndex = 0;
            this.txtStdID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStdID_KeyDown);
            this.txtStdID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStdID_KeyPress);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(118, 12);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "학번검색";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnSuccess
            // 
            this.btnSuccess.Location = new System.Drawing.Point(12, 234);
            this.btnSuccess.Name = "btnSuccess";
            this.btnSuccess.Size = new System.Drawing.Size(181, 30);
            this.btnSuccess.TabIndex = 4;
            this.btnSuccess.Text = "배틀신청";
            this.btnSuccess.UseVisualStyleBackColor = true;
            this.btnSuccess.Click += new System.EventHandler(this.btnSuccess_Click);
            // 
            // txtToken
            // 
            this.txtToken.Location = new System.Drawing.Point(56, 207);
            this.txtToken.Name = "txtToken";
            this.txtToken.Size = new System.Drawing.Size(137, 21);
            this.txtToken.TabIndex = 5;
            this.txtToken.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtToken.TextChanged += new System.EventHandler(this.txtToken_TextChanged);
            this.txtToken.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtToken_KeyPress);
            // 
            // lvwStuent
            // 
            this.lvwStuent.Location = new System.Drawing.Point(12, 41);
            this.lvwStuent.Name = "lvwStuent";
            this.lvwStuent.Size = new System.Drawing.Size(181, 160);
            this.lvwStuent.TabIndex = 8;
            this.lvwStuent.UseCompatibleStateImageBehavior = false;
            // 
            // lblToken
            // 
            this.lblToken.AutoSize = true;
            this.lblToken.Location = new System.Drawing.Point(10, 210);
            this.lblToken.Name = "lblToken";
            this.lblToken.Size = new System.Drawing.Size(41, 12);
            this.lblToken.TabIndex = 9;
            this.lblToken.Text = "포인트";
            // 
            // FormBattle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(204, 276);
            this.Controls.Add(this.lblToken);
            this.Controls.Add(this.lvwStuent);
            this.Controls.Add(this.txtToken);
            this.Controls.Add(this.btnSuccess);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtStdID);
            this.Name = "FormBattle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "배틀신청";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtStdID;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnSuccess;
        private System.Windows.Forms.TextBox txtToken;
        private System.Windows.Forms.ListView lvwStuent;
        private System.Windows.Forms.Label lblToken;
    }
}