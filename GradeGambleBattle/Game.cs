﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeGambleBattle
{
    public class Game
    {
        private Student user;
        private List<Student> student;
        private List<Subject> subject;
        private List<Battle> battle;

        private GameDAO dao;

        public Game(int userID)
        {
            this.dao = new GameDAO();

            this.user = null;
            this.student = new List<Student>();
            this.subject = new List<Subject>();
            this.battle = new List<Battle>();

            login(userID);
            update();
        }

        public void login(int userID)
        {
            DataTable userData = dao.getUser(userID);

            int id = Convert.ToInt32(userData.Rows[0]["stdID"]);
            string name = Convert.ToString(userData.Rows[0]["stdName"]);
            string major = Convert.ToString(userData.Rows[0]["stdMajor1"]);
            string major2 = Convert.ToString(userData.Rows[0]["stdMajor2"]);
            int years = Convert.ToInt32(userData.Rows[0]["stdYears"]);
            double grade = Convert.ToInt32(userData.Rows[0]["stdGrade"]);
            int token = Convert.ToInt32(userData.Rows[0]["stdToken"]);

            user = new Student(id, name, major, major2, years, grade, token);
        }

        public void update()
        {
            DataTable tbStudent = dao.getsStudent();
            DataTable tbSubject = dao.getsSubject();
            DataTable tbApply = dao.getsApplyOfUser(user.getID());
            DataTable tbBattle = dao.getsBattleOfUser(user.getID());
            
            List<Apply> apply = new List<Apply>();

            foreach (DataRow row in tbStudent.Rows)
            {
                int id = Convert.ToInt32(row["stdID"]);
                string name = Convert.ToString(row["stdName"]);
                string major = Convert.ToString(row["stdMajor1"]);
                string major2 = Convert.ToString(row["stdMajor2"]);
                int years = Convert.ToInt32(row["stdYears"]);
                double grade = Convert.ToInt32(row["stdGrade"]);
                int token = Convert.ToInt32(row["stdToken"]);

                this.student.Add(new Student(id, name, major, major2, years, grade, token));
            }

            foreach (DataRow row in tbSubject.Rows)
            {
                int subjectID = Convert.ToInt32(row["sbjID"]);
                string subjectName = Convert.ToString(row["sbjName"]);
                string professorName = Convert.ToString(row["pfName"]);
                int grade = Convert.ToInt32(row["sbjGrade"]);
                int limitNum = Convert.ToInt32(row["sbjLimitNum"]);
                int presentNum = Convert.ToInt32(row["sbjPresentNum"]);

                this.subject.Add(new Subject(subjectID, subjectName, professorName, grade, limitNum, presentNum));
            }

            foreach (DataRow row in tbApply.Rows)
            {
                int subjectID = Convert.ToInt32(row["sbjID"]);
                int middleScore = Convert.ToInt32(row["middleScore"]);
                int finalScore = Convert.ToInt32(row["finalScore"]);
                double grade = Convert.ToInt32(row["grade"]);

                for (IEnumerator j = subject.GetEnumerator(); j.MoveNext(); )
                {
                    Subject tmpSubject = (Subject)j.Current;

                    if (tmpSubject.getSubjectID() == subjectID)
                    {
                        user.addSubjet(new Apply(getUser().getID(), tmpSubject, middleScore, finalScore, grade));
                    }
                }
            }

            foreach (DataRow row in tbBattle.Rows)
            {
                int token = Convert.ToInt32(row["battingToken"]);
                bool isEnd = Convert.ToBoolean(row["isEnd"]);
                
                List<Student> lstStudent = new List<Student>();
                List<Student> lstAccept = new List<Student>();

                DataTable tbJoin = dao.getsJoinOfBattle(Convert.ToInt32(row["battleID"]));

                foreach (DataRow row2 in tbJoin.Rows)
                {
                    int id = Convert.ToInt32(row2["stdID"]);
                    bool isJoin = Convert.ToBoolean(row2["isEnd"]);

                    List<Student> student = getStudent();

                    for (IEnumerator i = student.GetEnumerator(); i.MoveNext(); )
                    {
                        Student tmpStudent = (Student)i.Current;

                        if (tmpStudent.getID() == id)
                        {
                            lstStudent.Add(tmpStudent);

                            if (isJoin)
                            {
                                lstAccept.Add(tmpStudent);
                            }

                            break;
                        }
                    }
                }

                if (lstStudent.Count == 2)
                {
                    this.battle.Add(new PersonalBattle(lstStudent, lstAccept, token, isEnd));
                }
                else
                {
                    this.battle.Add(new GroupBattle(lstStudent, lstAccept, token, isEnd));
                }
            }
        }

        public void delBattle(Battle battle)
        {
            this.battle.Remove(battle);
        }

        public Student getUser()
        {
            return this.user;
        }

        public List<Student> getStudent()
        {
            return this.student;
        }

        public List<Subject> getSubject()
        {
            return this.subject;
        }

        public List<Battle> getBattle()
        {
            return this.battle;
        }
    }
}
