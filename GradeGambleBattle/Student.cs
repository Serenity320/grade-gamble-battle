﻿using System;
using System.Collections; // IEnumerator
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeGambleBattle
{
    public class Student
    {
        private int id; // 학번
        private string name; // 이름
        private string major; // 학부
        private string major2; // 제1전공
        private int years; // 학년
        private double grade; // 학점
        private int token; // 토큰
        private List<Apply> apply; // 수강과목

        private StudentDAO dao;

        public Student(int id, string name, string major, string major2, int years, double grade, int token)
        {
            this.id = id;
            this.name = name;
            this.major = major;
            this.major2 = major2;
            this.years = years;
            this.grade = grade;
            this.token = token;
            this.apply = new List<Apply>();

            this.dao = new StudentDAO();
        }

        public void calGrade()
        {
            double sum = 0.0;

            for (IEnumerator i = apply.GetEnumerator(); i.MoveNext(); )
            {
                Apply tmpApply = (Apply)i.Current;

                sum += tmpApply.getGrade() * tmpApply.getSubject().getGrade();
            }

            grade = sum / calSubjectGrade();

            dao.setGrade(id, grade); // Database
        }        

        public int calSubjectGrade()
        {
            int grade = 0;

            for (IEnumerator i = apply.GetEnumerator(); i.MoveNext(); )
            {
                Apply tmpApply = (Apply)i.Current;

                grade += tmpApply.getSubject().getGrade();
            }

            return grade;
        }

        public void addToken(int token)
        {
            this.token += token;

            dao.setToken(id, this.token); // Database
        }

        public void subToken(int token)
        {
            this.token -= token;

            dao.setToken(id, this.token); // Database
        }

        public void addSubjet(Apply apply)
        {
            this.apply.Add(apply);
        }

        public void applySubjet(Apply apply)
        {
            this.apply.Add(apply);
            apply.getSubject().incPresentNum();
        }

        public void cancelSubject(Apply apply)
        {
            this.apply.Remove(apply);
            apply.getSubject().decPresentNum();
        }

        public bool checkSubject(Subject subject)
        {
            bool check = false;

            for (IEnumerator i = apply.GetEnumerator(); i.MoveNext(); )
            {
                Apply tmpApply = (Apply)i.Current;

                if (tmpApply.getSubject().Equals(subject))
                {
                    check = true;
                    break;
                }
            }

            return check;
        }

        public bool isStartMiddelExam()
        {
            bool isCheck = false;

            List<Apply> apply = this.apply;

            for (IEnumerator i = apply.GetEnumerator(); i.MoveNext(); )
            {
                Apply tmpApply = (Apply)i.Current;

                int middleScore = tmpApply.getMiddleScore();

                if (middleScore > 0)
                {
                    isCheck = true;
                    break;
                }
            }

            return isCheck;
        }

        public bool isStartFinalExam()
        {
            bool isCheck = true;

            List<Apply> apply = this.apply;

            for (IEnumerator i = apply.GetEnumerator(); i.MoveNext(); )
            {
                Apply tmpApply = (Apply)i.Current;

                int middleScore = tmpApply.getFinalScore();

                if (middleScore > 0)
                {
                    isCheck = false;
                    break;
                }
            }

            return isCheck;
        }

        public bool isEndMiddelExam()
        {
            bool isCheck = true;

            List<Apply> apply = this.apply;

            for (IEnumerator i = apply.GetEnumerator(); i.MoveNext(); )
            {
                Apply tmpApply = (Apply)i.Current;

                int middleScore = tmpApply.getMiddleScore();

                if (middleScore < 0)
                {
                    isCheck = false;
                    break;
                }
            }

            return isCheck;
        }

        public bool isEndFinalExam()
        {
            bool isCheck = true;

            List<Apply> apply = this.apply;

            for (IEnumerator i = apply.GetEnumerator(); i.MoveNext(); )
            {
                Apply tmpApply = (Apply)i.Current;

                int middleScore = tmpApply.getFinalScore();

                if (middleScore < 0)
                {
                    isCheck = false;
                    break;
                }
            }

            return isCheck;
        }

        public int getID()
        {
            return this.id;
        }

        public string getName()
        {
            return this.name;
        }

        public string getMajor()
        {
            return this.major;
        }

        public string getMajor2()
        {
            return this.major2;
        }

        public int getYears()
        {
            return this.years;
        }

        public double getGrade()
        {
            return this.grade;
        }

        public int getToken()
        {
            return this.token;
        }

        public List<Apply> getApply()
        {
            return this.apply;
        }
    }
}
