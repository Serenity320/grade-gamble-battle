﻿using System;
using System.Collections; // IEnumerator
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradeGambleBattle
{
    public partial class FormGamble : Form
    {
        private FormMain frmMain;
        private List<Battle> joinBattle;

        public FormGamble(FormMain frmMain)
        {
            InitializeComponent();

            this.frmMain = frmMain;
            this.joinBattle = new List<Battle>();

            update();
        }

        public void update()
        {
            // ListView
            lvwGamble.FocusedItem = null;
            lvwGamble.Clear();

            lvwGamble.View = View.Details;
            lvwGamble.FullRowSelect = true;
            lvwGamble.GridLines = true;

            lvwGamble.Columns.Add("참여");
            lvwGamble.Columns.Add("종류");
            lvwGamble.Columns.Add("신청");
            lvwGamble.Columns.Add("참여");
            lvwGamble.Columns.Add("포인트");
            lvwGamble.Columns.Add("참여자");
            
            List<Battle> battle = frmMain.getGame().getBattle();

            for (IEnumerator i = battle.GetEnumerator(); i.MoveNext(); )
            {
                Battle tmpBattle = (Battle)i.Current;

                joinBattle.Add(tmpBattle);

                string join = "X";
                List<Student> accept = tmpBattle.getAccept();

                for (IEnumerator j = accept.GetEnumerator(); j.MoveNext(); )
                {
                    Student tmpAccept = (Student)j.Current;

                    if (tmpAccept.getID() == frmMain.getGame().getUser().getID())
                    {
                        join = "O";
                    }
                }

                ListViewItem item = new ListViewItem(join);

                if (tmpBattle.getStudent().Count == 2)
                {
                    item.SubItems.Add("1:1");
                }
                else
                {
                    item.SubItems.Add("그룹");
                }

                item.SubItems.Add(Convert.ToString(tmpBattle.getStudent().Count));
                item.SubItems.Add(Convert.ToString(tmpBattle.getAccept().Count));
                item.SubItems.Add(Convert.ToString(tmpBattle.getToken()));

                string strJoin = "";

                List<Student> student = tmpBattle.getStudent();

                for (IEnumerator j = student.GetEnumerator(); j.MoveNext(); )
                {
                    Student tmpStudent = (Student)j.Current;

                    strJoin += tmpStudent.getName() + "(" + tmpStudent.getID() + "), ";
                }

                strJoin = strJoin.Substring(0, strJoin.Length - 2);
                item.SubItems.Add(strJoin);

                lvwGamble.Items.Add(item);
            }

            foreach (ColumnHeader column in lvwGamble.Columns)
            {
                column.Width = -2;
            }

            this.Controls.Add(lvwGamble);
        }

        private void btnGamble_Click(object sender, EventArgs e)
        {
            FormBattle frmBattle = new FormBattle(frmMain, this);
            frmBattle.Show();
        }

        private void btnBattleAccept_Click(object sender, EventArgs e)
        {
            int count = lvwGamble.SelectedItems.Count;

            if (count > 0)
            {
                if (frmMain.getGame().getUser().getGrade() >= 0)
                {
                    int idx = lvwGamble.FocusedItem.Index;
                    string join = lvwGamble.Items[idx].SubItems[0].Text;

                    Battle battle = joinBattle.ElementAt(idx);

                    if (frmMain.getGame().getUser().getToken() - battle.getToken() < 0)
                    {
                        MessageBox.Show("포인트 부족");
                    }
                    else if (join.Equals("X"))
                    {
                        battle.joinBattle(frmMain.getGame().getUser());
                        frmMain.update(); // 추가

                        MessageBox.Show("배틀참여 완료");
                    }
                    else if (battle.getEnd())
                    {
                        MessageBox.Show("종료된 배틀");
                    }
                    else
                    {
                        MessageBox.Show("이미 배틀참가");
                    }

                    if (battle.getStudent().Count == battle.getAccept().Count)
                    {
                        battle.allocate();

                        MessageBox.Show("배틀 후 포인트 분배");
                    }
                }
                else
                {
                    MessageBox.Show("모든 시험 후 배틀가능");
                }
            }

            update();
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            int count = lvwGamble.SelectedItems.Count;

            if (count > 0)
            {
                int idx = lvwGamble.FocusedItem.Index;
                string join = lvwGamble.Items[idx].SubItems[0].Text;

                Battle battle = joinBattle.ElementAt(idx);

                if (battle.getStudent().Count <= 2)
                {
                    frmMain.getGame().delBattle(battle);
                    battle.delBattle();
                }
                else if (battle.getStudent().Count <= 3)
                {
                    frmMain.getGame().delBattle(battle);
                    battle.delBattle();
                }
                else
                {
                    battle.rejectBattle(frmMain.getGame().getUser());
                }

                MessageBox.Show("배틀거절 완료");
            }

            update();
        }
    }
}
