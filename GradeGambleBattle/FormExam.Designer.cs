﻿namespace GradeGambleBattle
{
    partial class FormExam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            
            
            
            
            btnGame1 = new System.Windows.Forms.Button();
            this.lvwGrade = new System.Windows.Forms.ListView();
            this.btnEnd = new System.Windows.Forms.Button();
            this.lblGrade = new System.Windows.Forms.Label();
            this.btnGame2 = new System.Windows.Forms.Button();
            this.btnGame3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGame1
            // 
            this.btnGame1.Location = new System.Drawing.Point(12, 12);
            this.btnGame1.Name = "btnGame1";
            this.btnGame1.Size = new System.Drawing.Size(60, 30);
            this.btnGame1.TabIndex = 2;
            this.btnGame1.Text = "가위";
            this.btnGame1.UseVisualStyleBackColor = true;
            this.btnGame1.Click += new System.EventHandler(this.btnGame1_Click);
            // 
            // lvwGrade
            // 
            this.lvwGrade.Location = new System.Drawing.Point(12, 48);
            this.lvwGrade.Name = "lvwGrade";
            this.lvwGrade.Size = new System.Drawing.Size(192, 200);
            this.lvwGrade.TabIndex = 6;
            this.lvwGrade.UseCompatibleStateImageBehavior = false;
            // 
            // btnEnd
            // 
            this.btnEnd.Location = new System.Drawing.Point(90, 254);
            this.btnEnd.Name = "btnEnd";
            this.btnEnd.Size = new System.Drawing.Size(114, 30);
            this.btnEnd.TabIndex = 7;
            this.btnEnd.Text = "시험 종료";
            this.btnEnd.UseVisualStyleBackColor = true;
            this.btnEnd.Click += new System.EventHandler(this.btnEnd_Click);
            // 
            // lblGrade
            // 
            this.lblGrade.AutoSize = true;
            this.lblGrade.Location = new System.Drawing.Point(15, 263);
            this.lblGrade.Name = "lblGrade";
            this.lblGrade.Size = new System.Drawing.Size(47, 12);
            this.lblGrade.TabIndex = 9;
            this.lblGrade.Text = "점수 : 0";
            // 
            // btnGame2
            // 
            this.btnGame2.Location = new System.Drawing.Point(78, 12);
            this.btnGame2.Name = "btnGame2";
            this.btnGame2.Size = new System.Drawing.Size(60, 30);
            this.btnGame2.TabIndex = 10;
            this.btnGame2.Text = "바위";
            this.btnGame2.UseVisualStyleBackColor = true;
            this.btnGame2.Click += new System.EventHandler(this.btnGame2_Click);
            // 
            // btnGame3
            // 
            this.btnGame3.Location = new System.Drawing.Point(144, 12);
            this.btnGame3.Name = "btnGame3";
            this.btnGame3.Size = new System.Drawing.Size(60, 30);
            this.btnGame3.TabIndex = 11;
            this.btnGame3.Text = "보";
            this.btnGame3.UseVisualStyleBackColor = true;
            this.btnGame3.Click += new System.EventHandler(this.btnGame3_Click);
            // 
            // FormExam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(214, 291);
            this.Controls.Add(this.btnGame3);
            this.Controls.Add(this.btnGame2);
            this.Controls.Add(this.lblGrade);
            this.Controls.Add(this.btnEnd);
            this.Controls.Add(this.lvwGrade);
            this.Controls.Add(this.btnGame1);
            this.Name = "FormExam";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "중간고사";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGame1;
        private System.Windows.Forms.ListView lvwGrade;
        private System.Windows.Forms.Button btnEnd;
        private System.Windows.Forms.Label lblGrade;
        private System.Windows.Forms.Button btnGame2;
        private System.Windows.Forms.Button btnGame3;
    }
}