﻿using System;
using System.Collections.Generic;
using System.Data; // DataTable
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient; // MySql

namespace GradeGambleBattle
{
    public class DBConnector
    {
        private MySqlConnection conn;

        private string server;
        private string database;
        private string userID;
        private string password;

        public DBConnector()
        {
            conn = null;

            this.server = "";
            this.database = "";
            this.userID = "";
            this.password = "";
        }

        public void openDBC()
        {
            conn = new MySqlConnection("Server=" + server + ";Database=" + database + ";Uid=" + userID + ";Pwd=" + password + ";Charset=utf8");

            conn.Open();
        }

        public void closeDBC()
        {
            conn.Close();
        }

        public void sendQuery(string query)
        {
            openDBC();

            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.ExecuteNonQuery();

            closeDBC();
        }

        public DataTable selectQuery(string query)
        {
            openDBC();

            MySqlDataAdapter da = new MySqlDataAdapter(query, conn);
            DataTable dt = new DataTable();

            da.Fill(dt);

            closeDBC();

            return dt;
        }
    }
}
