﻿namespace GradeGambleBattle
{
    partial class FormGrade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFinalExam = new System.Windows.Forms.Button();
            this.lvwGrade = new System.Windows.Forms.ListView();
            this.btnMiddleExam = new System.Windows.Forms.Button();
            this.lblGrade = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnFinalExam
            // 
            this.btnFinalExam.Location = new System.Drawing.Point(408, 226);
            this.btnFinalExam.Name = "btnFinalExam";
            this.btnFinalExam.Size = new System.Drawing.Size(80, 25);
            this.btnFinalExam.TabIndex = 6;
            this.btnFinalExam.Text = "기말고사";
            this.btnFinalExam.UseVisualStyleBackColor = true;
            this.btnFinalExam.Click += new System.EventHandler(this.btnFinalExam_Click);
            // 
            // lvwGrade
            // 
            this.lvwGrade.Location = new System.Drawing.Point(0, 0);
            this.lvwGrade.Name = "lvwGrade";
            this.lvwGrade.Size = new System.Drawing.Size(500, 220);
            this.lvwGrade.TabIndex = 5;
            this.lvwGrade.UseCompatibleStateImageBehavior = false;
            // 
            // btnMiddleExam
            // 
            this.btnMiddleExam.Location = new System.Drawing.Point(322, 226);
            this.btnMiddleExam.Name = "btnMiddleExam";
            this.btnMiddleExam.Size = new System.Drawing.Size(80, 25);
            this.btnMiddleExam.TabIndex = 7;
            this.btnMiddleExam.Text = "중간고사";
            this.btnMiddleExam.UseVisualStyleBackColor = true;
            this.btnMiddleExam.Click += new System.EventHandler(this.btnMiddleExam_Click);
            // 
            // lblGrade
            // 
            this.lblGrade.AutoSize = true;
            this.lblGrade.Location = new System.Drawing.Point(12, 232);
            this.lblGrade.Name = "lblGrade";
            this.lblGrade.Size = new System.Drawing.Size(93, 12);
            this.lblGrade.TabIndex = 8;
            this.lblGrade.Text = "총 학점 : 평가중";
            // 
            // FormGrade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 251);
            this.Controls.Add(this.lblGrade);
            this.Controls.Add(this.btnMiddleExam);
            this.Controls.Add(this.btnFinalExam);
            this.Controls.Add(this.lvwGrade);
            this.Name = "FormGrade";
            this.Text = "FormgGrade";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFinalExam;
        private System.Windows.Forms.ListView lvwGrade;
        private System.Windows.Forms.Button btnMiddleExam;
        private System.Windows.Forms.Label lblGrade;
    }
}