﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradeGambleBattle
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string id = txtID.Text;
            string password = txtPassword.Text;

            if (string.IsNullOrEmpty(id))
            {
                MessageBox.Show("아이디를 입력하세요.");
            }
            else if (string.IsNullOrEmpty(password))
            {
                MessageBox.Show("비밀번호를 입력하세요.");
            }
            else
            {
                StudentDAO dao = new StudentDAO();

                if (dao.checkLogin(id, password) == 1)
                {
                    this.Visible = false;

                    FormMain frmMain = new FormMain(Convert.ToInt32(id));
                    frmMain.Show();
                }
                else
                {
                    MessageBox.Show("아이디/비밀번호 불일치");

                    txtID.Text = "";
                    txtPassword.Text = "";

                    txtID.Focus();
                }
            }
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnLogin_Click(sender, e);
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.btnLogin_Click(sender, e);
            }
        }
    }
}
