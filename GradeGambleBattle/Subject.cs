﻿using System;
using System.Collections.Generic;
using System.Data; // DataTable
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradeGambleBattle
{
    public class Subject
    {
        private int id; // 과목번호
        private string subjectName; // 교과목명
        private string professorName; // 교수이름
        private int grade; // 학점
        private int limitNum; // 제한인원
        private int presentNum; // 수강인원

        private SubjectDAO dao;

        public Subject(int subjectID, string subjectName, string professorName, int grade, int limitNum, int presentNum)
        {
            this.id = subjectID;
            this.subjectName = subjectName;
            this.professorName = professorName;
            this.grade = grade;
            this.limitNum = limitNum;
            this.presentNum = presentNum;

            this.dao = new SubjectDAO();
        }

        public void incPresentNum()
        {
            presentNum++;

            dao.incPresentNum(id); // Database
        }

        public void decPresentNum()
        {
            presentNum--;

            dao.decPresentNum(id); // Database
        }

        public int getSubjectID()
        {
            return this.id;
        }

        public string getSubjectName()
        {
            return this.subjectName;
        }

        public string getProfessorName()
        {
            return this.professorName;
        }

        public int getGrade()
        {
            return this.grade;
        }

        public int getLimitNum()
        {
            return this.limitNum;
        }

        public int getPresentNum()
        {
            return this.presentNum;
        }
    }
}
